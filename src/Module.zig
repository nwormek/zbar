const std = @import("std");
const alloc = &@import("root").alloc;

const Module = @This();

cmd: []const u8,

/// Interval in seconds. If zero, module will only be updated by signal.
interval: u32 = 0,

/// Signal number. If zero, module will only be updated by time.
signal: u6 = 0,

/// If this is set to true, the command will be run with the output name as the first argument for
/// each output
per_output: bool = false,

/// Returns an owned slice that must be freed.
/// Can't be called on modules with `per_output == true`, use `runForOutput()` instead
pub fn run(self: Module) []const u8 {
    const result = std.process.Child.run(.{
        .allocator = alloc.*,
        .argv = &.{self.cmd},
    }) catch return "";

    alloc.free(result.stderr);
    return result.stdout;
}

/// Same as `run()`, but for modules with `per_output == true`.
pub fn runForOutput(self: Module, output: []const u8) []const u8 {
    const result = std.process.Child.run(.{
        .allocator = alloc.*,
        .argv = &.{ self.cmd, output },
    }) catch return "";

    alloc.free(result.stderr);
    return result.stdout;
}
