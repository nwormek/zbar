const pixman = @import("pixman");
const fcft = @import("fcft");
const Module = @import("Module.zig");

pub const bg_active = rgba(0x000000dd);
pub const bg_inactive = rgba(0x000000aa);
pub const fg_active = rgb(0xffffff);
pub const fg_inactive = rgb(0xcccccc);

pub const color_scheme = [8]pixman.Color{
    // Selenized Dark
    rgb(0x252525), // black
    rgb(0xed4a46), // red
    rgb(0x70b433), // green
    rgb(0xdbb32d), // yellow
    rgb(0x368aeb), // blue
    rgb(0xeb6eb7), // magenta
    rgb(0x3fc5b7), // cyan
    rgb(0x777777), // white
};

pub const font_name = "monospace";
pub const font_size = 11;

pub const padding_x = 12;
pub const padding_y = 5;

pub const n_tags = 12;
pub const max_titlelen = 80;

pub const separator = "\x1b[37m | \x1b[m";

/// This command is used to determine the active output
pub const output_cmd = "";
/// `output_cmd` is run on this signal
pub const output_sig = 1;

pub const top = struct {
    pub const left = [_]Module{};
    pub const center = [_]Module{
        .{ .cmd = "sb-time", .interval = 5 },
    };
    pub const right = [_]Module{
        .{ .cmd = "sb-volume", .interval = 60, .signal = 11 },
    };
};

pub const bottom = struct {
    pub const left = [_]Module{};
    pub const center = [_]Module{};
    pub const right = [_]Module{
        .{ .cmd = "sb-media", .interval = 60, .signal = 13 },
    };
};

// Helper functions

fn rgb(hex: u24) pixman.Color {
    const r8 = (hex >> 16) & 0xff;
    const g8 = (hex >> 8) & 0xff;
    const b8 = (hex >> 0) & 0xff;

    return .{
        .red = r8 | (r8 << 8),
        .green = g8 | (g8 << 8),
        .blue = b8 | (b8 << 8),
        .alpha = 0xffff,
    };
}

fn rgba(hex: u32) pixman.Color {
    const r8 = (hex >> 24) & 0xff;
    const g8 = (hex >> 16) & 0xff;
    const b8 = (hex >> 8) & 0xff;
    const a8 = (hex >> 0) & 0xff;

    return .{
        .red = r8 | (r8 << 8),
        .green = g8 | (g8 << 8),
        .blue = b8 | (b8 << 8),
        .alpha = a8 | (a8 << 8),
    };
}
