const std = @import("std");
const pixman = @import("pixman");
const fcft = @import("fcft");

const config = @import("config");

const alloc: *std.mem.Allocator = &@import("root").alloc;

const Text = @This();

const DEFAULT_COLOR: u4 = 0b1000;

buf: []const u32,
/// every slice in segments is contained in buf
segments: []const []const u32,
colors: []const u4,

text_runs: ?[]*const fcft.TextRun = null,

pub fn init(str_view: []const u8) !Text {
    const trimmed_view = std.mem.trim(u8, str_view, " \t\n");
    const buf = try alloc.alloc(u32, trimmed_view.len);
    errdefer alloc.free(buf);
    var segments = std.ArrayList([]const u32).init(alloc.*);
    errdefer segments.deinit();
    var colors = std.ArrayList(u4).init(alloc.*);
    errdefer colors.deinit();

    var i: usize = 0;
    var segment_start: usize = 0;
    var current_color: u4 = DEFAULT_COLOR;
    var iter = (try std.unicode.Utf8View.init(trimmed_view)).iterator();
    while (iter.nextCodepoint()) |cp| {
        if (cp == '\x1b') {
            if (i != segment_start) {
                try segments.append(buf[segment_start..i]);
                try colors.append(current_color);
                segment_start = i;
            }
            const remaining = trimmed_view[iter.i..];
            if (remaining[0] != '[') return error.MalformedAnsi;
            const m_index = std.mem.indexOf(u8, remaining, "m") orelse
                return error.MalformedAnsi;
            const payload = remaining[1..m_index];

            const reset_color = payload.len == 0 or std.mem.eql(u8, payload, "0");
            if (reset_color) {
                current_color = DEFAULT_COLOR;
            } else {
                const num = try std.fmt.parseInt(u32, payload, 0);
                if (30 <= num and num <= 37) {
                    current_color = @truncate(num - 30);
                } else if (90 <= num and num <= 97) {
                    current_color = @truncate(num - 90);
                }
            }
            iter.i += 1 + m_index;
        } else {
            buf[i] = cp;
            i += 1;
        }
    }
    try segments.append(buf[segment_start..i]);
    try colors.append(current_color);

    return .{
        .buf = buf,
        .segments = try segments.toOwnedSlice(),
        .colors = try colors.toOwnedSlice(),
    };
}

pub fn arrEmpty(comptime N: usize) ![N]Text {
    var result: [N]Text = undefined;
    for (&result) |*r| r.* = try Text.init("");
    return result;
}

/// Asserts that `render()` has already been called
pub fn renderedLen(self: Text) u32 {
    std.debug.assert(self.text_runs != null);
    var len: i32 = 0;
    for (self.text_runs.?) |r| {
        const glyphs: []*const fcft.Glyph = r.glyphs[0..r.count];
        for (glyphs) |g| len += g.advance.x;
    }
    return @intCast(len);
}

pub fn cpLen(self: Text) u32 {
    var len: u32 = 0;
    for (self.segments) |s| len += @intCast(s.len);
    return len;
}

/// Renders the text run if it has not been rendered yet
pub fn render(self: *Text, font: *fcft.Font) !void {
    if (self.text_runs) |_| return;

    self.text_runs = try alloc.alloc(*const fcft.TextRun, self.segments.len);
    for (self.segments, self.text_runs.?) |s, *tr|
        tr.* = try font.rasterizeTextRunUtf32(s, .default);
}

pub fn rerender(self: *Text, font: *fcft.Font) !void {
    if (self.text_runs) |trs| {
        for (trs) |tr| tr.destroy();
    } else self.text_runs = try alloc.alloc(*const fcft.TextRun, self.segments.len);
    for (self.segments, self.text_runs.?) |s, *tr|
        tr.* = try font.rasterizeTextRunUtf32(s, .default);
}

pub fn draw(self: Text, image: *pixman.Image, active: bool, startx: i32, y: i32) !i32 {
    var x = startx;
    for (self.text_runs.?, self.colors) |tr, c| {
        // zig fmt: off
        const color: *const pixman.Color =
            if      (c <= 7) &config.color_scheme[c]
            else if (active) &config.fg_active
            else             &config.fg_inactive;
        // zig fmt: on
        const fg = pixman.Image.createSolidFill(color) orelse return error.PixmanFailed;
        defer _ = fg.unref();

        const glyphs = tr.glyphs[0..tr.count];
        for (glyphs) |g| {
            // Check for prerendered glyphs (emoji)
            if (g.pix.getFormat() == .a8r8g8b8) {
                // zig fmt: off
                pixman.Image.composite32(
                    .over, @ptrCast(g.pix), fg, image,
                    0, 0, 0, 0,
                    x + g.x, y - g.y,
                    g.width, g.height
                );
                // zig fmt: on
            } else {
                // zig fmt: off
                pixman.Image.composite32(
                    .over, fg, @ptrCast(g.pix), image,
                    0, 0, 0, 0,
                    x + g.x, y - g.y,
                    g.width, g.height
                );
                // zig fmt: on
            }
            x += g.advance.x;
        }
    }
    return x - startx;
}

pub fn drawMax(self: Text, image: *pixman.Image, active: bool, startx: i32, y: i32, max_x: i32) !i32 {
    var x: i32 = startx;
    outer: for (self.text_runs.?, self.colors) |tr, c| {
        // zig fmt: off
        const color: *const pixman.Color =
            if      (c <= 7) &config.color_scheme[c]
            else if (active) &config.fg_active
            else             &config.fg_inactive;
        // zig fmt: on
        const fg = pixman.Image.createSolidFill(color) orelse return error.PixmanFailed;
        defer _ = fg.unref();

        const glyphs = tr.glyphs[0..tr.count];
        for (glyphs) |g| {
            if (x + g.advance.x >= max_x) break :outer;
            // Check for prerendered glyphs (emoji)
            if (g.pix.getFormat() == .a8r8g8b8) {
                // zig fmt: off
                pixman.Image.composite32(
                    .over, @ptrCast(g.pix), fg, image,
                    0, 0, 0, 0,
                    x + g.x, y - g.y,
                    g.width, g.height
                );
                // zig fmt: on
            } else {
                // zig fmt: off
                pixman.Image.composite32(
                    .over, fg, @ptrCast(g.pix), image,
                    0, 0, 0, 0,
                    x + g.x, y - g.y,
                    g.width, g.height
                );
                // zig fmt: on
            }
            x += g.advance.x;
        }
    }
    return x - startx;
}

pub fn deinit(self: Text) void {
    alloc.free(self.colors);
    alloc.free(self.segments);
    alloc.free(self.buf);

    if (self.text_runs) |rs| {
        for (rs) |r| r.destroy();
        alloc.free(rs);
    }
}
