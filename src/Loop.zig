//! This file is _inspired_ by levee/Loop.zig

const std = @import("std");
const SIG = std.os.linux.SIG;
const c = @cImport({
    @cInclude("signal.h");
    @cInclude("unistd.h");
    @cInclude("sys/signalfd.h");
});

const config = @import("config");
const State = @import("State.zig");
const Bar = @import("Bar.zig");

const alloc: *std.mem.Allocator = &@import("root").alloc;

const Loop = @This();

interval: u32,
modulus: u32,
sigfd: std.posix.fd_t,

SIGRTMIN: u32,
SIGRTMAX: u32,

const state: *State = &@import("root").state;

pub fn init() !Loop {
    var interval: u32 = 0;
    var max_interval: u32 = 0; // TODO: use actual kgV for this

    // Who cares about portability amiright?
    const SIGRTMIN = blk: {
        const rtmin = c.__libc_current_sigrtmin();
        const trunc: i32 = @truncate(rtmin);
        const result: u32 = @intCast(trunc);
        std.log.info("SIGRTMIN == {}", .{result});
        break :blk result;
    };
    const SIGRTMAX = blk: {
        const rtmin = c.__libc_current_sigrtmax();
        const trunc: i32 = @truncate(rtmin);
        const result: u32 = @intCast(trunc);
        std.log.info("SIGRTMAX == {}", .{result});
        break :blk result;
    };

    var signals: c.sigset_t = undefined;
    _ = c.sigemptyset(&signals);
    _ = c.sigaddset(&signals, SIG.ALRM);

    const all_modules =
        config.top.left ++ config.top.center ++ config.top.right ++
        config.bottom.left ++ config.bottom.center ++ config.bottom.right;

    for (all_modules) |mod| {
        interval = gcd(interval, mod.interval);
        if (mod.interval > max_interval)
            max_interval = mod.interval;
        if (mod.signal != 0)
            _ = c.sigaddset(@ptrCast(&signals), @intCast(SIGRTMIN + mod.signal));
    }
    _ = c.sigaddset(@ptrCast(&signals), @intCast(SIGRTMIN + config.output_sig));

    // const sigfd = std.os.linux.signalfd(-1, &signals, std.os.linux.SFD.NONBLOCK);
    const sigfd = c.signalfd(-1, &signals, c.SFD_NONBLOCK);

    for (SIGRTMIN..SIGRTMAX) |i|
        _ = c.sigaddset(&signals, @intCast(i));
    _ = c.sigprocmask(SIG.BLOCK, &signals, null);

    const self = Loop{
        .interval = interval,
        .modulus = max_interval,
        .sigfd = @intCast(sigfd),
        .SIGRTMIN = SIGRTMIN,
        .SIGRTMAX = SIGRTMAX,
    };

    std.log.info("Loop.init(): {any}", .{self});
    return self;
}

pub fn run(self: Loop) !void {
    const POLL = std.posix.POLL;
    const siginfo_t = std.posix.siginfo_t;

    runAll();

    try std.posix.raise(SIG.ALRM);

    var pfds = [_]std.posix.pollfd{
        .{ .fd = self.sigfd, .events = POLL.IN, .revents = undefined },
        .{ .fd = state.fd, .events = POLL.IN, .revents = undefined },
    };

    var time: u32 = 0;
    while (true) {
        while (true) {
            const ret = state.display.dispatchPending();
            _ = state.display.flush();
            if (ret == .SUCCESS) break;
        }

        _ = try std.posix.poll(&pfds, -1);

        for (pfds) |pfd| {
            if (pfd.revents & (POLL.HUP | POLL.ERR) != 0)
                return;
        }

        // signals
        if (pfds[0].revents & POLL.IN != 0) {
            var si: siginfo_t = undefined;
            const si_slice: *[@sizeOf(siginfo_t)]u8 = @ptrCast(&si);

            if (std.posix.read(pfds[0].fd, si_slice)) |amount| {
                if (amount != @sizeOf(siginfo_t))
                    return error.ReadWrongAmount;
            } else |err| return err;

            const signo = si.signo;
            switch (signo) {
                SIG.ALRM => {
                    std.log.info("ALARM: time: {}", .{time});
                    _ = c.alarm(self.interval);
                    time += self.interval;
                    if (time > self.modulus)
                        time = 0;
                    runInterval(time);
                },
                else => {
                    runSignal(@as(u32, @intCast(signo)) - self.SIGRTMIN);
                },
            }
            try state.redrawAll();
        }

        // wayland
        if (pfds[1].revents & POLL.IN != 0) {
            const errno = state.display.dispatch();
            if (errno != .SUCCESS) return;
        }
        if (pfds[1].revents & POLL.OUT != 0) {
            const errno = state.display.flush();
            if (errno != .SUCCESS) return;
        }
    }
}

fn runAll() void {
    inline for (.{
        .{ .top, .left },
        .{ .top, .center },
        .{ .top, .right },
        .{ .bottom, .left },
        .{ .bottom, .center },
        .{ .bottom, .right },
    }) |tuple| {
        const barpos: Bar.BarPos, const pos: Bar.Pos = tuple;
        const modules = @field(@field(config, @tagName(barpos)), @tagName(pos));
        for (modules, 0..) |mod, i| {
            runModule(mod, barpos, pos, i);
        }
    }
}

fn runSignal(signo: u32) void {
    if (signo == config.output_sig) {
        if (std.process.Child.run(.{
            .allocator = alloc.*,
            .argv = &.{config.output_cmd},
        })) |result| {
            state.setActiveOutput(std.mem.trim(u8, result.stdout, " \t\n"));
            alloc.free(result.stdout);
            alloc.free(result.stderr);
        } else |_| {}
        return;
    }

    inline for (.{
        .{ .top, .left },
        .{ .top, .center },
        .{ .top, .right },
        .{ .bottom, .left },
        .{ .bottom, .center },
        .{ .bottom, .right },
    }) |tuple| {
        const barpos: Bar.BarPos, const pos: Bar.Pos = tuple;
        const modules = @field(@field(config, @tagName(barpos)), @tagName(pos));
        for (modules, 0..) |mod, i| {
            if (mod.signal == signo)
                runModule(mod, barpos, pos, i);
        }
    }
}

fn runInterval(time: u32) void {
    inline for (.{
        .{ .top, .left },
        .{ .top, .center },
        .{ .top, .right },
        .{ .bottom, .left },
        .{ .bottom, .center },
        .{ .bottom, .right },
    }) |tuple| {
        const barpos: Bar.BarPos, const pos: Bar.Pos = tuple;
        const modules = @field(@field(config, @tagName(barpos)), @tagName(pos));
        for (modules, 0..) |mod, i| {
            if (mod.interval != 0 and time % mod.interval == 0)
                runModule(mod, barpos, pos, i);
        }
    }
}

fn runModule(mod: anytype, comptime barpos: Bar.BarPos, comptime pos: Bar.Pos, index: usize) void {
    // mod is anytype, because we can't import Module.zig here
    if (mod.per_output) {
        for (state.bars.items) |bar| {
            const output_name = bar.output_name;
            const stdout = mod.runForOutput(output_name);
            bar.setText(barpos, pos, index, stdout);
            alloc.free(stdout);
        }
    } else {
        const stdout = mod.run();
        for (state.bars.items) |bar|
            bar.setText(barpos, pos, index, stdout);
        alloc.free(stdout);
    }
}

fn gcd(a_: u32, b_: u32) u32 {
    var tmp: u32 = undefined;
    var a = a_;
    var b = b_;
    while (a != 0) {
        tmp = a;
        a = b % a;
        b = tmp;
    }
    return b;
}
