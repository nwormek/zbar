const std = @import("std");
const wl = @import("wayland").client.wl;
const zwlr = @import("wayland").client.zwlr;
const wp = @import("wayland").client.wp;

const config = @import("config");
const Bar = @import("Bar.zig");

const alloc = &@import("root").alloc;

const State = @This();

const SetFlags = packed struct {
    shm: bool = false,
    compositor: bool = false,
    layer_shell: bool = false,
    viewporter: bool = false,
    frac_scale_manager: bool = false,

    pub fn allSet(self: SetFlags) bool {
        return inline for (comptime std.meta.fieldNames(SetFlags)) |name| {
            if (!@field(self, name)) break false;
        } else true;
    }
};

display: *wl.Display,
fd: std.posix.fd_t,
registry: *wl.Registry,
shm: *wl.Shm = undefined,
compositor: *wl.Compositor = undefined,
layer_shell: *zwlr.LayerShellV1 = undefined,
viewporter: *wp.Viewporter = undefined,
frac_scale_manager: *wp.FractionalScaleManagerV1 = undefined,

set_flags: SetFlags = .{},

bars: std.ArrayList(*Bar),

pub fn init() !State {
    const display = try wl.Display.connect(null);
    return .{
        .display = display,
        .fd = @intCast(display.getFd()),
        .registry = try display.getRegistry(),
        .bars = std.ArrayList(*Bar).init(alloc.*),
    };
}

pub fn setup(self: *State) !void {
    self.registry.setListener(*State, registryListener, self);
    if (self.display.roundtrip() != .SUCCESS)
        return error.RoundtripFailed;

    if (!self.set_flags.allSet())
        return error.MissingFeatures;
}

pub fn deinit(self: *State) void {
    self.shm.destroy();
    self.compositor.destroy();
    self.layer_shell.destroy();

    for (self.bars.items) |bar|
        bar.destroy();
    self.bars.deinit();

    self.registry.destroy();
    self.display.disconnect();
}

pub fn redrawAll(self: State) !void {
    for (self.bars.items) |bar| {
        try bar.drawAll();
    }
}

pub fn setActiveOutput(self: State, name: []const u8) void {
    std.log.debug("setActiveOutput(): {s}", .{name});
    for (self.bars.items) |bar| {
        std.log.debug(" - {s}", .{bar.output_name});
        bar.active = std.mem.eql(u8, name, bar.output_name);
    }
}

fn registryListener(registry: *wl.Registry, event: wl.Registry.Event, self: *State) void {
    switch (event) {
        .global => |data| {
            const ifaceIs = struct {
                pub fn fun(gdata: anytype, comptime itype: type) bool {
                    return std.mem.orderZ(u8, gdata.interface, itype.getInterface().name) == .eq;
                }
            }.fun;

            if (ifaceIs(data, wl.Shm)) {
                self.shm = registry.bind(data.name, wl.Shm, 1) catch unreachable;
                self.set_flags.shm = true;
            } else if (ifaceIs(data, wl.Compositor)) {
                self.compositor = registry.bind(data.name, wl.Compositor, 4) catch unreachable;
                self.set_flags.compositor = true;
            } else if (ifaceIs(data, wl.Output)) {
                const new_bar = Bar.create(
                    registry.bind(data.name, wl.Output, 4) catch unreachable,
                ) catch unreachable;
                self.bars.append(new_bar) catch unreachable;
            } else if (ifaceIs(data, zwlr.LayerShellV1)) {
                self.layer_shell = registry.bind(data.name, zwlr.LayerShellV1, 3) catch unreachable;
                self.set_flags.layer_shell = true;
            } else if (ifaceIs(data, wp.Viewporter)) {
                self.viewporter = registry.bind(data.name, wp.Viewporter, 1) catch unreachable;
                self.set_flags.viewporter = true;
            } else if (ifaceIs(data, wp.FractionalScaleManagerV1)) {
                self.frac_scale_manager =
                    registry.bind(data.name, wp.FractionalScaleManagerV1, 1) catch unreachable;
                self.set_flags.frac_scale_manager = true;
            }
        },
        .global_remove => {},
    }
}
