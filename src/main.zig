const std = @import("std");
const pixman = @import("pixman");
const fcft = @import("fcft");
const wl = @import("wayland").client.wl;
const zwlr = @import("wayland").client.zwlr;
const zdwl = @import("wayland").client.zdwl;

const Bar = @import("Bar.zig");
const Loop = @import("Loop.zig");
const State = @import("State.zig");

pub var state: State = undefined;
pub var alloc: std.mem.Allocator = undefined;

pub fn main() !void {
    var gpa = std.heap.GeneralPurposeAllocator(.{}){};
    defer _ = gpa.deinit();
    alloc = gpa.allocator();

    _ = fcft.init(.auto, false, .warning);

    state = try State.init();
    defer state.deinit();

    state.setup() catch |e| {
        std.log.err("could not setup state: {}", .{e});
        return;
    };

    const loop = try Loop.init();
    try loop.run();
}
