const std = @import("std");
const fcft = @import("fcft");
const pixman = @import("pixman");
const wl = @import("wayland").client.wl;
const wp = @import("wayland").client.wp;
const zwlr = @import("wayland").client.zwlr;

const config = @import("config");

const alloc = &@import("root").alloc;
const state = &@import("root").state;

const Text = @import("Text.zig");

const Bar = @This();

pub const BarPos = enum { top, bottom };
pub const Pos = enum { left, center, right };

wl_output: *wl.Output,

// TODO:
//  - don't include `show`, instead: empty modules (modules returning empty strings?) ==> no show

wl_surface_top: *wl.Surface,
layer_surface_top: *zwlr.LayerSurfaceV1,
viewport_top: *wp.Viewport,

wl_surface_bottom: *wl.Surface,
layer_surface_bottom: *zwlr.LayerSurfaceV1,
viewport_bottom: *wp.Viewport,

/// The fractional scale belongs to _one_ surface, but we use it for both, since they are on the
/// same output.
frac_scale: *wp.FractionalScaleV1,

output_name: []const u8 = "",

scale_over_120: u32 = 120,
width: u31 = 0,
height: u31 = 0,

font: *fcft.Font,

active: bool = true,

modules: struct {
    separator: Text,
    top_left: [config.top.left.len]Text,
    top_center: [config.top.center.len]Text,
    top_right: [config.top.right.len]Text,
    bottom_left: [config.bottom.left.len]Text,
    bottom_center: [config.bottom.center.len]Text,
    bottom_right: [config.bottom.right.len]Text,
},

pub fn create(wl_output: *wl.Output) !*Bar {
    const self: *Bar = try alloc.create(Bar);

    // TODO: If bar is created too early, the objects in state are not ready.
    //       Maybe consider splitting this into separate functions, e.g.
    //       - create(wl_output) for allocating
    //       - init(self) which can be called _after_ the roundtrip
    const wl_surface_top = try state.compositor.createSurface();
    errdefer wl_surface_top.destroy();
    const layer_surface_top =
        try state.layer_shell.getLayerSurface(wl_surface_top, wl_output, .bottom, "zbar");
    errdefer layer_surface_top.destroy();
    const viewport_top = try state.viewporter.getViewport(wl_surface_top);
    errdefer viewport_top.destroy();

    const wl_surface_bottom = try state.compositor.createSurface();
    errdefer wl_surface_bottom.destroy();
    const layer_surface_bottom =
        try state.layer_shell.getLayerSurface(wl_surface_bottom, wl_output, .bottom, "zbar");
    errdefer layer_surface_bottom.destroy();
    const viewport_bottom = try state.viewporter.getViewport(wl_surface_bottom);
    errdefer viewport_bottom.destroy();

    const frac_scale = try state.frac_scale_manager.getFractionalScale(wl_surface_top);
    errdefer frac_scale.destroy();

    var initial_font_arr =
        [_][*:0]const u8{std.fmt.comptimePrint("{s}:size={}", .{ config.font_name, config.font_size })};
    const initial_font = try fcft.Font.fromName(&initial_font_arr, "dpi=96");
    const initial_height: u31 =
        @truncate(2 * config.padding_y + @as(u32, @intCast(initial_font.height)));

    self.* = .{
        .wl_output = wl_output,

        .wl_surface_top = wl_surface_top,
        .layer_surface_top = layer_surface_top,
        .viewport_top = viewport_top,

        .wl_surface_bottom = wl_surface_bottom,
        .layer_surface_bottom = layer_surface_bottom,
        .viewport_bottom = viewport_bottom,

        .frac_scale = frac_scale,

        .font = initial_font,

        .modules = .{
            .separator = try Text.init(config.separator),
            .top_left = try Text.arrEmpty(config.top.left.len),
            .top_center = try Text.arrEmpty(config.top.center.len),
            .top_right = try Text.arrEmpty(config.top.right.len),
            .bottom_left = try Text.arrEmpty(config.bottom.left.len),
            .bottom_center = try Text.arrEmpty(config.bottom.center.len),
            .bottom_right = try Text.arrEmpty(config.bottom.right.len),
        },
    };

    self.modules.separator.render(self.font) catch {};

    self.wl_output.setListener(*Bar, outputListener, self);

    const top_height = initial_height;
    self.layer_surface_top.setAnchor(.{ .top = true, .left = true, .right = true, .bottom = false });
    self.layer_surface_top.setSize(0, top_height);
    self.layer_surface_top.setExclusiveZone(top_height);
    self.layer_surface_top.setListener(*Bar, layerSurfaceListener, self);

    const bottom_height = initial_height;
    self.layer_surface_bottom.setAnchor(.{ .top = false, .left = true, .right = true, .bottom = true });
    self.layer_surface_bottom.setSize(0, bottom_height);
    self.layer_surface_bottom.setExclusiveZone(bottom_height);
    self.layer_surface_bottom.setListener(*Bar, layerSurfaceListener, self);

    self.frac_scale.setListener(*Bar, fractionalScaleListener, self);

    self.wl_surface_top.commit();
    self.wl_surface_bottom.commit();

    return self;
}

pub fn destroy(self: *Bar) void {
    self.wl_output.destroy();

    self.wl_surface_top.destroy();
    self.layer_surface_top.destroy();
    self.viewport_top.destroy();

    self.wl_surface_bottom.destroy();
    self.layer_surface_bottom.destroy();
    self.viewport_bottom.destroy();

    self.frac_scale.destroy();

    self.font.destroy();

    inline for (comptime std.meta.fields(@TypeOf(self.modules))) |field| {
        if (field.type == Text) {
            const t: *Text = &@field(self.modules, field.name);
            t.deinit();
        } else {
            const ts: []Text = &@field(self.modules, field.name);
            for (ts) |*t| t.deinit();
        }
    }

    alloc.destroy(self);
}

pub fn setText(
    self: *Bar,
    comptime barpos: Bar.BarPos,
    comptime pos: Bar.Pos,
    index: usize,
    str: []const u8,
) void {
    const text = Text.init(str) catch return;
    const field_name = @tagName(barpos) ++ "_" ++ @tagName(pos);
    const text_ptr = &@field(self.modules, field_name)[index];
    text_ptr.deinit();
    text_ptr.* = text;
}

fn outputListener(_: *wl.Output, event: wl.Output.Event, self: *Bar) void {
    switch (event) {
        .name => |data| {
            // This leaks if output name is set more than once
            const new_name: []const u8 = std.mem.span(data.name);
            self.output_name = alloc.dupe(u8, new_name) catch return;
            std.log.debug("output_name: {s}", .{self.output_name});
        },
        else => {},
    }
}

fn layerSurfaceListener(surface: *zwlr.LayerSurfaceV1, event: zwlr.LayerSurfaceV1.Event, self: *Bar) void {
    std.debug.assert(surface == self.layer_surface_top or surface == self.layer_surface_bottom);
    const is_top = surface == self.layer_surface_top;

    switch (event) {
        .configure => |data| {
            self.height = @truncate(data.height);
            self.width = @truncate(data.width);
            surface.ackConfigure(data.serial);
            if (is_top) {
                self.viewport_top.setDestination(self.width, self.height);
                self.draw(.top) catch |e|
                    std.log.err("failed to draw frame: {}", .{e});
            } else {
                self.viewport_bottom.setDestination(self.width, self.height);
                self.draw(.bottom) catch |e|
                    std.log.err("failed to draw frame: {}", .{e});
            }
        },
        .closed => {},
    }
}

fn updateScale(self: *Bar, new_scale: u32) enum { Same, Err, Changed } {
    if (self.scale_over_120 == new_scale)
        return .Same;

    self.scale_over_120 = new_scale;
    const scaled_font_size: f64 = (config.font_size * @as(f64, @floatFromInt(new_scale))) / 120;
    const font_str = std.fmt.allocPrintZ(
        alloc.*,
        "{s}:size={}",
        .{ config.font_name, scaled_font_size },
    ) catch |e| {
        std.log.err("could not alloc font_str: {}", .{e});
        return .Err;
    };
    var font_arr = [_][*:0]u8{font_str};
    defer alloc.free(font_str);
    if (fcft.Font.fromName(&font_arr, "dpi=96")) |font| {
        self.font.destroy();
        self.font = font;
    } else |e| {
        std.log.err("could not create font from string ('{s}'): {}", .{ font_str, e });
        return .Err;
    }

    const font_height: u32 = @intCast(self.font.height);
    const unscaled_font_size = font_height * 120 / self.scale_over_120;
    const height: u31 = @truncate(2 * config.padding_y + unscaled_font_size);

    self.layer_surface_top.setSize(0, height);
    self.layer_surface_top.setExclusiveZone(height);

    self.layer_surface_bottom.setSize(0, height);
    self.layer_surface_bottom.setExclusiveZone(height);

    inline for (comptime std.meta.fields(@TypeOf(self.modules))) |field| {
        if (field.type == Text) {
            const t: *Text = &@field(self.modules, field.name);
            t.rerender(self.font) catch {};
        } else {
            const ts: []Text = &@field(self.modules, field.name);
            for (ts) |*t| t.rerender(self.font) catch {};
        }
    }

    return .Changed;
}

fn fractionalScaleListener(_: *wp.FractionalScaleV1, event: wp.FractionalScaleV1.Event, self: *Bar) void {
    switch (event) {
        .preferred_scale => |data| {
            if (self.updateScale(data.scale) != .Changed)
                return;
            self.drawAll() catch |e| std.log.err("failed to draw frame: {}", .{e});
        },
    }
}

fn bufferListener(buffer: *wl.Buffer, event: wl.Buffer.Event, _: *Bar) void {
    switch (event) {
        .release => buffer.destroy(),
    }
}

pub fn drawAll(self: *Bar) !void {
    try self.draw(.top);
    try self.draw(.bottom);
}

pub fn draw(self: *Bar, comptime pos: BarPos) !void {
    const posix = std.posix;
    const linux = std.os.linux;

    if (self.height == 0 or self.width == 0) return;

    const surface = switch (pos) {
        .top => self.wl_surface_top,
        .bottom => self.wl_surface_bottom,
    };

    const bytes_per_pixel: u31 = 4;
    const width: u31 = @truncate(self.scale_over_120 * self.width / 120);
    const height: u31 = @truncate(self.scale_over_120 * self.height / 120);
    const stride = width * bytes_per_pixel;
    const size = stride * height;

    const fd = try posix.memfd_create("zbar", linux.MFD.CLOEXEC);
    defer posix.close(fd);
    try posix.ftruncate(fd, @intCast(size));

    const prot = linux.PROT.READ | linux.PROT.WRITE;
    const data = try posix.mmap(null, size, prot, .{ .TYPE = .SHARED }, fd, 0);
    defer posix.munmap(data);

    const pool = try state.shm.createPool(fd, size);
    defer pool.destroy();

    const buf = try pool.createBuffer(0, width, height, stride, .argb8888);
    buf.setListener(*Bar, bufferListener, self);

    const image =
        pixman.Image.createBits(.a8r8g8b8, width, height, @ptrCast(data), stride) orelse
        return error.PixmanFailed;

    const bg_color = if (self.active) &config.bg_active else &config.bg_inactive;
    _ = pixman.Image.fillRectangles(.src, image, bg_color, 1, &[_]pixman.Rectangle16{
        .{ .x = 0, .y = 0, .width = @truncate(width), .height = @truncate(height) },
    });

    const c_width = try self.drawTexts(pos, .center, self.width, image);
    _ = c_width;

    const l_width = try self.drawTexts(pos, .left, self.width, image);
    _ = l_width;

    const r_width = try self.drawTexts(pos, .right, self.width, image);
    _ = r_width;

    surface.attach(buf, 0, 0);
    surface.setBufferScale(1);
    surface.commit();
    surface.damage(0, 0, self.width, self.height);
}

fn drawTexts(
    self: *Bar,
    comptime bar: BarPos,
    comptime pos: Pos,
    max_width: u32,
    base: *pixman.Image,
) !u32 {
    const field_name = comptime @tagName(bar) ++ "_" ++ @tagName(pos);
    const texts: []Text = &@field(self.modules, field_name);
    const scaled_padding: u31 = @truncate(config.padding_x * self.scale_over_120 / 120);
    const scaled_width: u31 = @truncate(self.width * self.scale_over_120 / 120);

    var total_len: u32 = 0;
    var showsep = false;
    for (texts) |*text| {
        if (text.cpLen() == 0) continue;
        if (showsep) total_len += self.modules.separator.renderedLen();

        try text.render(self.font);
        total_len += text.renderedLen();
        showsep = true;
    }

    const y: i32 = self.font.ascent + @as(u31, @truncate(config.padding_y * self.scale_over_120 / 120));

    if (total_len < max_width) {
        const startx = switch (pos) {
            .left => scaled_padding,
            .center => (scaled_width - total_len) / 2,
            .right => scaled_width - scaled_padding - total_len,
        };

        var x: i32 = @intCast(startx);

        showsep = false;
        for (texts) |text| {
            if (text.cpLen() == 0) continue;
            if (showsep)
                x += try self.modules.separator.draw(base, self.active, x, y);

            x += try text.draw(base, self.active, x, y);
            showsep = true;
        }

        const xu: u32 = @intCast(x);

        return xu - startx;
    } else {
        // TODO: use actual length of truncated text
        const startx = switch (pos) {
            .left => scaled_padding,
            .center => (scaled_width -| max_width) / 2,
            .right => scaled_padding - scaled_padding -| max_width,
        };
        const istartx: i32 = @intCast(startx);

        var w: i32 = 0;

        showsep = false;
        for (texts) |text| {
            if (text.cpLen() == 0) continue;
            if (showsep) {
                if (@as(u32, @intCast(w)) + self.modules.separator.renderedLen() >= max_width) break;
                w += try self.modules.separator.draw(base, self.active, istartx + w, y);
            }

            w += try text.drawMax(base, self.active, istartx + w, y, @intCast(startx + max_width));
            showsep = true;
        }

        return @intCast(w);
    }
}
