# zbar

A simple status bar for dwl written in zig

## Configuration

The status bar is configured at compile time:
Copy `src/config.default.zig` to `src/config.zig` and adjust to your liking.
